/**
 * @file
 * Register a template definition set named "default".
 */

CKEDITOR.addTemplates('default',
{
    // The name of the subfolder that contains the preview images of the templates.
    // imagesPath : CKEDITOR.getUrl( CKEDITOR.plugins.getPath( 'templates' ) + 'templates/images/' ),.
    // Template definitions.
    templates :
        [
            {
                title: '2 columns, 50/50',
                description: 'Columns are 340px wide',
                html:
                    '<div class="columns-wrapper two clearfix">' +
          '<div class="column"><p>First column content. Add your text here.</p></div>' +
                    '<div class="column"><p>Second column content. Add your text here.</p></div>' +
          '</div>'
            },
            {
                title: '3 columns, 33/33/33',
                description: 'Columns are 220px wide',
                html:
                    '<div class="columns-wrapper three clearfix">' +
          '<div class="column"><p>First column content. Add your text here.</p></div>' +
                    '<div class="column"><p>Second column content. Add your text here.</p></div>' +
          '<div class="column"><p>Third column content. Add your text here.</p></div>' +
          '</div>'
            },
        ]
});
