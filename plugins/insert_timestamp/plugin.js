/**
 * @file
 * Insert Timestamp plugin.
 */

(function () {

    var insertTimestampCmd =
    {
        canUndo : false,    // The undo snapshot will be handled by 'insertElement'.
        exec : function (editor) {

            var date = new Date();
      var timestamp = date.toDateString() + ' ' + date.toTimeString() + '&nbsp;-&nbsp;';
            editor.insertHtml(timestamp);
        }
    };

    var pluginName = 'inserttimestamp';

    // Register a plugin named "InsertTimestamp".
    CKEDITOR.plugins.add(pluginName,
    {
        init : function (editor) {

            editor.addCommand(pluginName, insertTimestampCmd);
            editor.ui.addButton('inserttimestamp',
                {
                    label : "Insert current time",
                    command : pluginName,
          icon: this.path + 'timestamp.png'
                });
        }
    });
})();
